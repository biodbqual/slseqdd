
Results
======

Correctly Classified Instances      870457               77.511  %
Incorrectly Classified Instances    252554               22.489  %
Kappa statistic                          0.5476
K&B Relative Info Score            61774567.2349 %
K&B Information Score               617649.4855 bits      0.55   bits/instance
Class complexity | order 0         1122836.1396 bits      0.9998 bits/instance
Class complexity | scheme          271242996 bits    241.5319 bits/instance
Complexity improvement     (Sf)    -270120159.8604 bits   -240.5321 bits/instance
Mean absolute error                      0.2249
Root mean squared error                  0.4742
Relative absolute error                 44.9877 %
Root relative squared error             94.8554 %
Coverage of cases (0.95 level)          77.511  %
Mean rel. region size (0.95 level)      50      %
Total Number of Instances          1123011     

=== Confusion Matrix ===

      a      b   <-- classified as
 318435 234821 |      a = DU
  17733 552022 |      b = DI

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.576    0.031    0.947      0.576    0.716      0.594    0.772     0.754     DU
                 0.969    0.424    0.702      0.969    0.814      0.594    0.772     0.696     DI
Weighted Avg.    0.775    0.231    0.823      0.775    0.766      0.594    0.772     0.724     

