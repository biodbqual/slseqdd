Naive Bayes Classifier

                    Class
Attribute              DU       DI
                   (0.49)   (0.51)
===================================
Description
  mean              0.6997   0.2187
  std. dev.         0.1929   0.1533
  weight sum        553256   569755
  precision         0.0111   0.0111

Has_Literature
  NO               14355.0 265682.0
  YES             538903.0 304075.0
  [total]         553258.0 569757.0

Literature
  mean              0.4408    0.004
  std. dev.         0.1606   0.0443
  weight sum        553256   569755
  precision         0.0357   0.0357

Submitter
  False            53236.0 432918.0
  True            492854.0  46120.0
  N/A               7169.0  90720.0
  [total]         553259.0 569758.0

Length
  mean              0.7976   0.4512
  std. dev.         0.2922   0.2692
  weight sum        553256   569755
  precision           0.01     0.01

SEQ_HIT
  YES             553257.0 569660.0
  NO                   1.0     97.0
  [total]         553258.0 569757.0

SEQ_AP
  mean              0.6066   0.0089
  std. dev.         0.4554   0.0142
  weight sum        553256   569755
  precision           0.01     0.01

SEQ_ID
  mean              0.9622   0.9084
  std. dev.         0.0568   0.0774
  weight sum        553256   569755
  precision          0.027    0.027

SEQ_EX
  mean              0.0709   0.4596
  std. dev.         0.2187   0.7294
  weight sum        553256   569755
  precision         0.0066   0.0066

SEQ_THRE
  NO              422949.0  57412.0
  YES             130309.0 512345.0
  [total]         553258.0 569757.0

HAS_CD
  YES             552587.0 562530.0
  NO                 671.0   7227.0
  [total]         553258.0 569757.0

CD_HIT
  YES             552578.0 562118.0
  NO                 680.0   7639.0
  [total]         553258.0 569757.0

CD_AP
  mean              0.6266   0.0118
  std. dev.         0.4577   0.0168
  weight sum        553256   569755
  precision           0.01     0.01

CD_ID
  mean                0.96   0.8977
  std. dev.         0.0725   0.1293
  weight sum        553256   569755
  precision          0.027    0.027

CD_EX
  mean              0.0892   0.6227
  std. dev.         0.3303   0.9559
  weight sum        553256   569755
  precision         0.0068   0.0068

CD_THRE
  NO              411139.0  11471.0
  YES             142119.0 558286.0
  [total]         553258.0 569757.0

HAS_TR
  YES             552587.0 562530.0
  NO                 671.0   7227.0
  [total]         553258.0 569757.0

TR_HIT
  YES             522852.0 464027.0
  NO               30406.0 105730.0
  [total]         553258.0 569757.0

TR_AP
  mean               0.651   0.0154
  std. dev.         0.4488   0.0228
  weight sum        553256   569755
  precision           0.01     0.01

TR_ID
  mean              0.7856   0.3398
  std. dev.         0.3318   0.2194
  weight sum        553256   569755
  precision         0.0108   0.0108

TR_EX
  mean              1.3364   2.5318
  std. dev.          2.323   2.2097
  weight sum        553256   569755
  precision         0.0057   0.0057

TR_THRE
  NO              388226.0   3434.0
  YES             165032.0 566323.0
  [total]         553258.0 569757.0


