J48 pruned tree
------------------

SEQ_AP <= 0.89
|   SEQ_EX <= 0.0005
|   |   SEQ_AP <= 0
|   |   |   SEQ_ID <= 0.97
|   |   |   |   Length <= 0.02
|   |   |   |   |   SEQ_EX <= 0.00001
|   |   |   |   |   |   Description <= 0.24: DI (10.0/4.0)
|   |   |   |   |   |   Description > 0.24: EF (6.0)
|   |   |   |   |   SEQ_EX > 0.00001: DI (22.0)
|   |   |   |   Length > 0.02: DI (879.0/32.0)
|   |   |   SEQ_ID > 0.97
|   |   |   |   SEQ_EX <= 0.00001: EF (242.0/6.0)
|   |   |   |   SEQ_EX > 0.00001: DI (18.0)
|   |   SEQ_AP > 0
|   |   |   SEQ_ID <= 0.74
|   |   |   |   SEQ_AP <= 0.01: DI (49.0/7.0)
|   |   |   |   SEQ_AP > 0.01
|   |   |   |   |   SEQ_EX <= 0.00004: EF (7.0/1.0)
|   |   |   |   |   SEQ_EX > 0.00004: DI (2.0)
|   |   |   SEQ_ID > 0.74
|   |   |   |   Length <= 0.42
|   |   |   |   |   SEQ_AP <= 0.01
|   |   |   |   |   |   SEQ_ID <= 0.8
|   |   |   |   |   |   |   Length <= 0.05: EF (15.0)
|   |   |   |   |   |   |   Length > 0.05: DI (11.0/2.0)
|   |   |   |   |   |   SEQ_ID > 0.8
|   |   |   |   |   |   |   Literature <= 0.25
|   |   |   |   |   |   |   |   HAS_CD = NO: EF (437.0/2.0)
|   |   |   |   |   |   |   |   HAS_CD = YES: DI (8.0)
|   |   |   |   |   |   |   Literature > 0.25: EF (294.0/4.0)
|   |   |   |   |   SEQ_AP > 0.01: EF (1477.0)
|   |   |   |   Length > 0.42
|   |   |   |   |   SEQ_AP <= 0.02
|   |   |   |   |   |   Literature <= 0: DI (26.0)
|   |   |   |   |   |   Literature > 0
|   |   |   |   |   |   |   SEQ_ID <= 0.8
|   |   |   |   |   |   |   |   Submitter = N/A: DI (0.0)
|   |   |   |   |   |   |   |   Submitter = False
|   |   |   |   |   |   |   |   |   Description <= 0.6
|   |   |   |   |   |   |   |   |   |   Length <= 0.8: DI (4.0)
|   |   |   |   |   |   |   |   |   |   Length > 0.8: EF (3.0)
|   |   |   |   |   |   |   |   |   Description > 0.6: DI (24.0/2.0)
|   |   |   |   |   |   |   |   Submitter = True: EF (7.0/1.0)
|   |   |   |   |   |   |   SEQ_ID > 0.8
|   |   |   |   |   |   |   |   Description <= 0.6
|   |   |   |   |   |   |   |   |   SEQ_ID <= 0.96
|   |   |   |   |   |   |   |   |   |   SEQ_ID <= 0.92
|   |   |   |   |   |   |   |   |   |   |   SEQ_ID <= 0.87: DI (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   SEQ_ID > 0.87: EF (11.0/2.0)
|   |   |   |   |   |   |   |   |   |   SEQ_ID > 0.92: DI (3.0)
|   |   |   |   |   |   |   |   |   SEQ_ID > 0.96: EF (7.0)
|   |   |   |   |   |   |   |   Description > 0.6: EF (135.0/18.0)
|   |   |   |   |   SEQ_AP > 0.02: EF (420.0/2.0)
|   SEQ_EX > 0.0005
|   |   TR_THRE = YES
|   |   |   Submitter = N/A
|   |   |   |   Description <= 0.43: DI (453.0/14.0)
|   |   |   |   Description > 0.43
|   |   |   |   |   Literature <= 0.25: DI (7.0)
|   |   |   |   |   Literature > 0.25: LI (3.0)
|   |   |   Submitter = False: DI (3001.0/37.0)
|   |   |   Submitter = True
|   |   |   |   SEQ_AP <= 0: DI (11.0/2.0)
|   |   |   |   SEQ_AP > 0: LI (13.0/1.0)
|   |   TR_THRE = NO
|   |   |   TR_ID <= 0.47
|   |   |   |   CD_AP <= 0: LI (3.0/1.0)
|   |   |   |   CD_AP > 0: DI (4.0)
|   |   |   TR_ID > 0.47: LI (86.0)
SEQ_AP > 0.89: ES (1243.0)

Number of Leaves  : 	37

Size of the tree : 	71

