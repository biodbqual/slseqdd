Relation Name:  complete-weka.filters.unsupervised.attribute.Remove-R1,24-weka.filters.supervised.attribute.NominalToBinary-weka.filters.unsupervised.attribute.Normalize-S1.0-T0.0
Num Instances:  8946
Num Attributes: 25

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
 1 Description                Num   0%   4%  96%     0 /  0%     3 /  0%    75 
 2 Has_Literature=NO          Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 3 Literature                 Num   0% 100%   0%     0 /  0%     1 /  0%     5 
 4 Submitter=N/A              Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 5 Submitter=False            Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 6 Submitter=True             Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 7 Length                     Num   0%  12%  88%     0 /  0%     0 /  0%   101 
 8 SEQ_HIT                    Num   0% 100%   0%     0 /  0%     0 /  0%     1 
 9 SEQ_AP                     Num   0%  51%  49%     0 /  0%     2 /  0%   101 
10 SEQ_ID                     Num   0%  22%  78%     0 /  0%     0 /  0%    36 
11 SEQ_EX                     Num   0%  52%  48%     0 /  0%   405 /  5%   913 
12 SEQ_THRE=YES               Num   0% 100%   0%     0 /  0%     0 /  0%     2 
13 HAS_CD=YES                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
14 CD_HIT=YES                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
15 CD_AP                      Num   0%  72%  28%     0 /  0%     7 /  0%    97 
16 CD_ID                      Num   0%  77%  23%     0 /  0%     2 /  0%    32 
17 CD_EX                      Num   0%  20%  80%     0 /  0%   109 /  1%   334 
18 CD_THRE=NO                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
19 HAS_TR=YES                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
20 TR_HIT=YES                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
21 TR_AP                      Num   0%  73%  27%     0 /  0%    17 /  0%    97 
22 TR_ID                      Num   0%  75%  25%     0 /  0%     6 /  0%    72 
23 TR_EX                      Num   0%  21%  79%     0 /  0%   191 /  2%   431 
24 TR_THRE=NO                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
25 CAT                        Nom 100%   0%   0%     0 /  0%     0 /  0%     4 

