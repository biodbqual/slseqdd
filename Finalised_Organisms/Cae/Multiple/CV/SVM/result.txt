
Results
======

Correctly Classified Instances        8272               92.4659 %
Incorrectly Classified Instances       674                7.5341 %
Kappa statistic                          0.877 
K&B Relative Info Score             786208.3378 %
K&B Information Score                12004.6753 bits      1.3419 bits/instance
Class complexity | order 0           13656.2428 bits      1.5265 bits/instance
Class complexity | scheme           723876      bits     80.9162 bits/instance
Complexity improvement     (Sf)    -710219.7572 bits    -79.3896 bits/instance
Mean absolute error                      0.0377
Root mean squared error                  0.1941
Relative absolute error                 12.305  %
Root relative squared error             49.6113 %
Coverage of cases (0.95 level)          92.4659 %
Mean rel. region size (0.95 level)      25      %
Total Number of Instances             8946     

=== Confusion Matrix ===

    a    b    c    d   <-- classified as
 2782   82    0  210 |    a = EF
    1 1242    0    0 |    b = ES
    0    0   88   67 |    c = LI
  309    0    5 4160 |    d = DI

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.905    0.053    0.900      0.905    0.902      0.851    0.926     0.847     EF
                 0.999    0.011    0.938      0.999    0.968      0.963    0.994     0.937     ES
                 0.568    0.001    0.946      0.568    0.710      0.730    0.784     0.545     LI
                 0.930    0.062    0.938      0.930    0.934      0.868    0.934     0.907     DI
Weighted Avg.    0.925    0.051    0.925      0.925    0.924      0.873    0.937     0.884     

