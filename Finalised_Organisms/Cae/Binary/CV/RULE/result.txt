
Results
======

Correctly Classified Instances        5762               64.4087 %
Incorrectly Classified Instances      3184               35.5913 %
Kappa statistic                          0.2881
K&B Relative Info Score             257799.9541 %
K&B Information Score                 2577.9994 bits      0.2882 bits/instance
Class complexity | order 0            8945.9997 bits      1      bits/instance
Class complexity | scheme          3419616      bits    382.2508 bits/instance
Complexity improvement     (Sf)    -3410670.0003 bits   -381.2508 bits/instance
Mean absolute error                      0.3559
Root mean squared error                  0.5966
Relative absolute error                 71.1827 %
Root relative squared error            119.3169 %
Coverage of cases (0.95 level)          64.4087 %
Mean rel. region size (0.95 level)      50      %
Total Number of Instances             8946     

=== Confusion Matrix ===

    a    b   <-- classified as
 1351 3121 |    a = DU
   63 4411 |    b = DI

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.302    0.014    0.955      0.302    0.459      0.395    0.644     0.638     DU
                 0.986    0.698    0.586      0.986    0.735      0.395    0.644     0.584     DI
Weighted Avg.    0.644    0.356    0.770      0.644    0.597      0.395    0.644     0.611     

