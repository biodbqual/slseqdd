
Results
======

Correctly Classified Instances        8258               92.3094 %
Incorrectly Classified Instances       688                7.6906 %
Kappa statistic                          0.8462
K&B Relative Info Score             757000.1014 %
K&B Information Score                 7570.0007 bits      0.8462 bits/instance
Class complexity | order 0            8946.0007 bits      1      bits/instance
Class complexity | scheme           738912      bits     82.5969 bits/instance
Complexity improvement     (Sf)    -729965.9993 bits    -81.5969 bits/instance
Mean absolute error                      0.0769
Root mean squared error                  0.2773
Relative absolute error                 15.3812 %
Root relative squared error             55.4638 %
Coverage of cases (0.95 level)          92.3094 %
Mean rel. region size (0.95 level)      50      %
Total Number of Instances             8946     

=== Confusion Matrix ===

    a    b   <-- classified as
 4115  357 |    a = DU
  331 4143 |    b = DI

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.920    0.074    0.926      0.920    0.923      0.846    0.923     0.892     DU
                 0.926    0.080    0.921      0.926    0.923      0.846    0.923     0.890     DI
Weighted Avg.    0.923    0.077    0.923      0.923    0.923      0.846    0.923     0.891     

