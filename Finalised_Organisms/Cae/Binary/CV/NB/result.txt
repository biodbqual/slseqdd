
Results
======

Correctly Classified Instances        7241               80.9412 %
Incorrectly Classified Instances      1705               19.0588 %
Kappa statistic                          0.6188
K&B Relative Info Score             553654.9356 %
K&B Information Score                 5536.5491 bits      0.6189 bits/instance
Class complexity | order 0            8946.0007 bits      1      bits/instance
Class complexity | scheme            38185.3709 bits      4.2684 bits/instance
Complexity improvement     (Sf)     -29239.3701 bits     -3.2684 bits/instance
Mean absolute error                      0.1906
Root mean squared error                  0.434 
Relative absolute error                 38.1174 %
Root relative squared error             86.7903 %
Coverage of cases (0.95 level)          81.5001 %
Mean rel. region size (0.95 level)      50.6204 %
Total Number of Instances             8946     

=== Confusion Matrix ===

    a    b   <-- classified as
 2843 1629 |    a = DU
   76 4398 |    b = DI

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.636    0.017    0.974      0.636    0.769      0.660    0.910     0.919     DU
                 0.983    0.364    0.730      0.983    0.838      0.660    0.910     0.908     DI
Weighted Avg.    0.809    0.191    0.852      0.809    0.803      0.660    0.910     0.914     

