J48 pruned tree
------------------

SEQ_EX <= 0.00007
|   SEQ_AP <= 0
|   |   SEQ_ID <= 0.97
|   |   |   Length <= 0.03
|   |   |   |   Length <= 0.01
|   |   |   |   |   SEQ_EX <= 0.00001: DU (14.0/4.0)
|   |   |   |   |   SEQ_EX > 0.00001: DI (4.0)
|   |   |   |   Length > 0.01: DI (12.0)
|   |   |   Length > 0.03: DI (667.0/23.0)
|   |   SEQ_ID > 0.97: DU (245.0/9.0)
|   SEQ_AP > 0
|   |   SEQ_ID <= 0.79
|   |   |   SEQ_AP <= 0.01
|   |   |   |   Submitter = N/A: DI (5.0)
|   |   |   |   Submitter = False
|   |   |   |   |   Length <= 0.05: DU (9.0)
|   |   |   |   |   Length > 0.05
|   |   |   |   |   |   Description <= 0.6
|   |   |   |   |   |   |   Length <= 0.81: DI (21.0/5.0)
|   |   |   |   |   |   |   Length > 0.81: DU (3.0)
|   |   |   |   |   |   Description > 0.6: DI (55.0/3.0)
|   |   |   |   Submitter = True: DU (8.0)
|   |   |   SEQ_AP > 0.01: DU (161.0/2.0)
|   |   SEQ_ID > 0.79
|   |   |   SEQ_AP <= 0.01
|   |   |   |   Length <= 0.36: DU (684.0/10.0)
|   |   |   |   Length > 0.36
|   |   |   |   |   Literature <= 0.25: DI (14.0)
|   |   |   |   |   Literature > 0.25
|   |   |   |   |   |   SEQ_ID <= 0.83
|   |   |   |   |   |   |   Submitter = N/A: DI (0.0)
|   |   |   |   |   |   |   Submitter = False: DI (16.0/4.0)
|   |   |   |   |   |   |   Submitter = True: DU (3.0)
|   |   |   |   |   |   SEQ_ID > 0.83: DU (118.0/16.0)
|   |   |   SEQ_AP > 0.01
|   |   |   |   SEQ_AP <= 0.03
|   |   |   |   |   HAS_CD = NO: DU (578.0/3.0)
|   |   |   |   |   HAS_CD = YES
|   |   |   |   |   |   CD_THRE = YES: DI (4.0)
|   |   |   |   |   |   CD_THRE = NO: DU (10.0)
|   |   |   |   SEQ_AP > 0.03: DU (2347.0)
SEQ_EX > 0.00007
|   CD_AP <= 0.03
|   |   TR_AP <= 0.1
|   |   |   TR_THRE = YES
|   |   |   |   CD_AP <= 0.01: DI (3624.0/63.0)
|   |   |   |   CD_AP > 0.01
|   |   |   |   |   Literature <= 0.25
|   |   |   |   |   |   Length <= 0.17
|   |   |   |   |   |   |   Description <= 0.33: DI (5.0)
|   |   |   |   |   |   |   Description > 0.33: DU (5.0/1.0)
|   |   |   |   |   |   Length > 0.17: DI (86.0)
|   |   |   |   |   Literature > 0.25: DU (10.0/1.0)
|   |   |   TR_THRE = NO
|   |   |   |   Length <= 0.58: DU (9.0/1.0)
|   |   |   |   Length > 0.58: DI (4.0)
|   |   TR_AP > 0.1: DU (41.0)
|   CD_AP > 0.03
|   |   Has_Literature = YES
|   |   |   Description <= 0.44
|   |   |   |   TR_THRE = YES: DI (6.0)
|   |   |   |   TR_THRE = NO: DU (19.0)
|   |   |   Description > 0.44: DU (157.0)
|   |   Has_Literature = NO: DI (2.0)

Number of Leaves  : 	34

Size of the tree : 	65

