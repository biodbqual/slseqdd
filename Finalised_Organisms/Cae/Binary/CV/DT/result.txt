
Results
======

Correctly Classified Instances        8772               98.055  %
Incorrectly Classified Instances       174                1.945  %
Kappa statistic                          0.9611
K&B Relative Info Score             841694.6481 %
K&B Information Score                 8416.9461 bits      0.9409 bits/instance
Class complexity | order 0            8946.0007 bits      1      bits/instance
Class complexity | scheme            16008.2575 bits      1.7894 bits/instance
Complexity improvement     (Sf)      -7062.2568 bits     -0.7894 bits/instance
Mean absolute error                      0.0332
Root mean squared error                  0.1345
Relative absolute error                  6.6335 %
Root relative squared error             26.893  %
Coverage of cases (0.95 level)          98.5915 %
Mean rel. region size (0.95 level)      51.6208 %
Total Number of Instances             8946     

=== Confusion Matrix ===

    a    b   <-- classified as
 4359  113 |    a = DU
   61 4413 |    b = DI

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.975    0.014    0.986      0.975    0.980      0.961    0.987     0.986     DU
                 0.986    0.025    0.975      0.986    0.981      0.961    0.987     0.979     DI
Weighted Avg.    0.981    0.019    0.981      0.981    0.981      0.961    0.987     0.983     

