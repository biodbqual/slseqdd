Relation Name:  complete-weka.filters.unsupervised.attribute.Remove-R1,24-weka.filters.supervised.attribute.NominalToBinary-weka.filters.unsupervised.attribute.Normalize-S1.0-T0.0
Num Instances:  9884
Num Attributes: 25

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
 1 Description                Num   0%   3%  97%     0 /  0%     2 /  0%    88 
 2 Has_Literature=YES         Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 3 Literature                 Num   0% 100%   0%     0 /  0%     1 /  0%     5 
 4 Submitter=False            Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 5 Submitter=True             Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 6 Submitter=N/A              Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 7 Length                     Num   0%   5%  95%     0 /  0%     0 /  0%   101 
 8 SEQ_HIT=NO                 Num   0% 100%   0%     0 /  0%     1 /  0%     2 
 9 SEQ_AP                     Num   0%  33%  67%     0 /  0%     0 /  0%   101 
10 SEQ_ID                     Num   0%  29%  71%     0 /  0%     2 /  0%    36 
11 SEQ_EX                     Num   0%  65%  35%     0 /  0%   457 /  5%  1170 
12 SEQ_THRE=YES               Num   0% 100%   0%     0 /  0%     0 /  0%     2 
13 HAS_CD=YES                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
14 CD_HIT=YES                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
15 CD_AP                      Num   0%  53%  47%     0 /  0%     1 /  0%   101 
16 CD_ID                      Num   0%  57%  43%     0 /  0%     2 /  0%    34 
17 CD_EX                      Num   0%  26%  74%     0 /  0%   217 /  2%   550 
18 CD_THRE=NO                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
19 HAS_TR=YES                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
20 TR_HIT=YES                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
21 TR_AP                      Num   0%  55%  45%     0 /  0%     2 /  0%   101 
22 TR_ID                      Num   0%  55%  45%     0 /  0%     6 /  0%    83 
23 TR_EX                      Num   0%  26%  74%     0 /  0%   321 /  3%   741 
24 TR_THRE=NO                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
25 CAT                        Nom 100%   0%   0%     0 /  0%     0 /  0%     4 

