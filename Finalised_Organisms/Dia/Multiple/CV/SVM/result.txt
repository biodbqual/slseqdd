
Results
======

Correctly Classified Instances        8675               87.7681 %
Incorrectly Classified Instances      1209               12.2319 %
Kappa statistic                          0.7913
K&B Relative Info Score             782701.1663 %
K&B Information Score                10888.4539 bits      1.1016 bits/instance
Class complexity | order 0           13743.8923 bits      1.3905 bits/instance
Class complexity | scheme          1298466      bits    131.3705 bits/instance
Complexity improvement     (Sf)    -1284722.1077 bits   -129.98   bits/instance
Mean absolute error                      0.0612
Root mean squared error                  0.2473
Relative absolute error                 21.1745 %
Root relative squared error             65.0805 %
Coverage of cases (0.95 level)          87.7681 %
Mean rel. region size (0.95 level)      25      %
Total Number of Instances             9884     

=== Confusion Matrix ===

    a    b    c    d   <-- classified as
 3836  136    0   45 |    a = EF
    1  835    0    0 |    b = ES
    1    0    0   88 |    c = LI
  938    0    0 4004 |    d = DI

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.955    0.160    0.803      0.955    0.873      0.781    0.897     0.785     EF
                 0.999    0.015    0.860      0.999    0.924      0.920    0.992     0.859     ES
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.009     LI
                 0.810    0.027    0.968      0.810    0.882      0.794    0.892     0.879     DI
Weighted Avg.    0.878    0.080    0.883      0.878    0.874      0.792    0.899     0.831     

