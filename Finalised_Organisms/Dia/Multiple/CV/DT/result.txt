
Results
======

Correctly Classified Instances        9461               95.7204 %
Incorrectly Classified Instances       423                4.2796 %
Kappa statistic                          0.9256
K&B Relative Info Score             891701.975  %
K&B Information Score                12404.8056 bits      1.255  bits/instance
Class complexity | order 0           13743.8923 bits      1.3905 bits/instance
Class complexity | scheme            47788.0118 bits      4.8349 bits/instance
Complexity improvement     (Sf)     -34044.1195 bits     -3.4444 bits/instance
Mean absolute error                      0.0297
Root mean squared error                  0.1302
Relative absolute error                 10.2711 %
Root relative squared error             34.2673 %
Coverage of cases (0.95 level)          98.6443 %
Mean rel. region size (0.95 level)      28.5057 %
Total Number of Instances             9884     

=== Confusion Matrix ===

    a    b    c    d   <-- classified as
 3850    0    1  166 |    a = EF
    0  836    0    0 |    b = ES
    1    0   28   60 |    c = LI
  184    0   11 4747 |    d = DI

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.958    0.032    0.954      0.958    0.956      0.926    0.989     0.977     EF
                 1.000    0.000    1.000      1.000    1.000      1.000    1.000     1.000     ES
                 0.315    0.001    0.700      0.315    0.434      0.466    0.888     0.309     LI
                 0.961    0.046    0.955      0.961    0.958      0.915    0.983     0.971     DI
Weighted Avg.    0.957    0.036    0.956      0.957    0.956      0.923    0.986     0.970     

