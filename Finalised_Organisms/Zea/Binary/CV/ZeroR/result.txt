
Results
======

Correctly Classified Instances       16105               50.1807 %
Incorrectly Classified Instances     15989               49.8193 %
Kappa statistic                          0     
K&B Relative Info Score                  0      %
K&B Information Score                    0      bits      0      bits/instance
Class complexity | order 0           32093.6978 bits      1      bits/instance
Class complexity | scheme            32093.6978 bits      1      bits/instance
Complexity improvement     (Sf)          0      bits      0      bits/instance
Mean absolute error                      0.5   
Root mean squared error                  0.5   
Relative absolute error                100      %
Root relative squared error            100      %
Coverage of cases (0.95 level)         100      %
Mean rel. region size (0.95 level)     100      %
Total Number of Instances            32094     

=== Confusion Matrix ===

     a     b   <-- classified as
 16105     0 |     a = DU
 15989     0 |     b = DI

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 1.000    1.000    0.502      1.000    0.668      0.000    0.500     0.502     DU
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.498     DI
Weighted Avg.    0.502    0.502    0.252      0.502    0.335      0.000    0.500     0.500     

