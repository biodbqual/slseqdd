J48 pruned tree
------------------

SEQ_AP <= 0.05
|   SEQ_EX <= 0.00007
|   |   Length <= 0.15
|   |   |   SEQ_ID <= 0.93: DI (18.0)
|   |   |   SEQ_ID > 0.93
|   |   |   |   SEQ_ID <= 0.98
|   |   |   |   |   SEQ_AP <= 0
|   |   |   |   |   |   Length <= 0: EF (4.0)
|   |   |   |   |   |   Length > 0: DI (6.0)
|   |   |   |   |   SEQ_AP > 0: EF (21.0/2.0)
|   |   |   |   SEQ_ID > 0.98
|   |   |   |   |   Length <= 0.03: EF (190.0/1.0)
|   |   |   |   |   Length > 0.03
|   |   |   |   |   |   SEQ_AP <= 0.02: DI (2.0)
|   |   |   |   |   |   SEQ_AP > 0.02: EF (15.0/1.0)
|   |   Length > 0.15
|   |   |   TR_ID <= 0.91: DI (288.0/2.0)
|   |   |   TR_ID > 0.91
|   |   |   |   CD_ID <= 0.94: DI (4.0)
|   |   |   |   CD_ID > 0.94: EF (9.0)
|   SEQ_EX > 0.00007
|   |   SEQ_AP <= 0.04
|   |   |   TR_EX <= 0.00001
|   |   |   |   TR_ID <= 0.84: DI (18.0)
|   |   |   |   TR_ID > 0.84: LI (15.0)
|   |   |   TR_EX > 0.00001: DI (15617.0/17.0)
|   |   SEQ_AP > 0.04
|   |   |   CD_EX <= 0.6: DI (4.0)
|   |   |   CD_EX > 0.6: LI (26.0)
SEQ_AP > 0.05
|   SEQ_AP <= 0.89
|   |   SEQ_ID <= 0.94
|   |   |   CD_ID <= 0.83: DI (32.0/1.0)
|   |   |   CD_ID > 0.83
|   |   |   |   Submitter = N/A
|   |   |   |   |   Length <= 0.96: EF (43.0/1.0)
|   |   |   |   |   Length > 0.96: DI (3.0/1.0)
|   |   |   |   Submitter = True
|   |   |   |   |   SEQ_ID <= 0.91: DI (2.0)
|   |   |   |   |   SEQ_ID > 0.91: EF (6.0/1.0)
|   |   |   |   Submitter = False
|   |   |   |   |   TR_AP <= 0.85: EF (3.0)
|   |   |   |   |   TR_AP > 0.85: DI (5.0/1.0)
|   |   SEQ_ID > 0.94: EF (10656.0/3.0)
|   SEQ_AP > 0.89: ES (5107.0/3.0)

Number of Leaves  : 	24

Size of the tree : 	46

