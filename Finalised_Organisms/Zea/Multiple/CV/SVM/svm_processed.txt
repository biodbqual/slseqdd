Relation Name:  complete-weka.filters.unsupervised.attribute.Remove-R1,24-weka.filters.supervised.attribute.NominalToBinary-weka.filters.unsupervised.attribute.Normalize-S1.0-T0.0
Num Instances:  32094
Num Attributes: 25

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
 1 Description                Num   0%   9%  91%     0 /  0%     2 /  0%    73 
 2 Has_Literature=NO          Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 3 Literature                 Num   0% 100%   0%     0 /  0%     2 /  0%     9 
 4 Submitter=N/A              Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 5 Submitter=True             Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 6 Submitter=False            Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 7 Length                     Num   0%   9%  91%     0 /  0%     0 /  0%   101 
 8 SEQ_HIT=NO                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 9 SEQ_AP                     Num   0%  20%  80%     0 /  0%     1 /  0%   101 
10 SEQ_ID                     Num   0%  19%  81%     0 /  0%     0 /  0%    35 
11 SEQ_EX                     Num   0%  51%  49%     0 /  0%   163 /  1%   493 
12 SEQ_THRE=YES               Num   0% 100%   0%     0 /  0%     0 /  0%     2 
13 HAS_CD=NO                  Num   0% 100%   0%     0 /  0%     0 /  0%     2 
14 CD_HIT=NO                  Num   0% 100%   0%     0 /  0%     0 /  0%     2 
15 CD_AP                      Num   0%  31%  69%     0 /  0%    15 /  0%    86 
16 CD_ID                      Num   0%  30%  70%     0 /  0%     0 /  0%    35 
17 CD_EX                      Num   0%  49%  51%     0 /  0%   130 /  0%   456 
18 CD_THRE=YES                Num   0% 100%   0%     0 /  0%     0 /  0%     2 
19 HAS_TR=NO                  Num   0% 100%   0%     0 /  0%     0 /  0%     2 
20 TR_HIT=NO                  Num   0% 100%   0%     0 /  0%     0 /  0%     2 
21 TR_AP                      Num   0%  31%  69%     0 /  0%    13 /  0%    87 
22 TR_ID                      Num   0%  36%  64%     0 /  0%     8 /  0%    81 
23 TR_EX                      Num   0%  49%  51%     0 /  0%   130 /  0%   527 
24 TR_THRE=YES                Num   0% 100%   0%     0 /  0%     0 /  0%     2 
25 CAT                        Nom 100%   0%   0%     0 /  0%     0 /  0%     4 

