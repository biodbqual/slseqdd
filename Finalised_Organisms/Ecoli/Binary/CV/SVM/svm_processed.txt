Relation Name:  complete-weka.filters.unsupervised.attribute.Remove-R1,25-weka.filters.supervised.attribute.NominalToBinary-weka.filters.unsupervised.attribute.Normalize-S1.0-T0.0
Num Instances:  2082
Num Attributes: 25

     Name                      Type  Nom  Int Real     Missing      Unique  Dist
 1 Description                Num   0%   9%  91%     0 /  0%     7 /  0%    68 
 2 Has_Literature=NO          Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 3 Literature                 Num   0%  97%   3%     0 /  0%     6 /  0%    20 
 4 Submitter=False            Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 5 Submitter=N/A              Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 6 Submitter=True             Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 7 Length                     Num   0%   7%  93%     0 /  0%     0 /  0%   101 
 8 SEQ_HIT=NO                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
 9 SEQ_AP                     Num   0%  27%  73%     0 /  0%    13 /  1%    99 
10 SEQ_ID                     Num   0%  31%  69%     0 /  0%     4 /  0%    35 
11 SEQ_EX                     Num   0%  51%  49%     0 /  0%   140 /  7%   336 
12 SEQ_THRE=YES               Num   0% 100%   0%     0 /  0%     0 /  0%     2 
13 HAS_CD=NO                  Num   0% 100%   0%     0 /  0%     0 /  0%     2 
14 CD_HIT=NO                  Num   0% 100%   0%     0 /  0%     0 /  0%     2 
15 CD_AP                      Num   0%  16%  84%     0 /  0%    16 /  1%    59 
16 CD_ID                      Num   0%  44%  56%     0 /  0%     2 /  0%    29 
17 CD_EX                      Num   0%  23%  77%     0 /  0%    93 /  4%   315 
18 CD_THRE=NO                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
19 HAS_TR=NO                  Num   0% 100%   0%     0 /  0%     0 /  0%     2 
20 TR_HIT=NO                  Num   0% 100%   0%     0 /  0%     0 /  0%     2 
21 TR_AP                      Num   0%  38%  62%     0 /  0%    22 /  1%    64 
22 TR_ID                      Num   0%  42%  58%     0 /  0%    12 /  1%    66 
23 TR_EX                      Num   0%  22%  78%     0 /  0%   142 /  7%   343 
24 TR_THRE=NO                 Num   0% 100%   0%     0 /  0%     0 /  0%     2 
25 CLASS                      Nom 100%   0%   0%     0 /  0%     0 /  0%     2 

