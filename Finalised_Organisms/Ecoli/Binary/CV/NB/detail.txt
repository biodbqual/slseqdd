Naive Bayes Classifier

                   Class
Attribute             DU      DI
                   (0.5)   (0.5)
=================================
Description
  mean             0.2824  0.1122
  std. dev.         0.261  0.0833
  weight sum         1042    1040
  precision        0.0149  0.0149

Has_Literature
  YES              1008.0  1003.0
  NO                 36.0    39.0
  [total]          1044.0  1042.0

Literature
  mean             0.0794       0
  std. dev.        0.1745  0.0044
  weight sum         1042    1040
  precision        0.0263  0.0263

Submitter
  False             466.0   263.0
  N/A               409.0   777.0
  True              170.0     3.0
  [total]          1045.0  1043.0

Length
  mean              0.516  0.4211
  std. dev.         0.343  0.2807
  weight sum         1042    1040
  precision          0.01    0.01

SEQ_HIT
  YES              1043.0  1039.0
  NO                  1.0     3.0
  [total]          1044.0  1042.0

SEQ_AP
  mean             0.3647  0.0081
  std. dev.        0.3424  0.0202
  weight sum         1042    1040
  precision        0.0102  0.0102

SEQ_ID
  mean             0.9926  0.9091
  std. dev.        0.0227   0.085
  weight sum         1042    1040
  precision        0.0294  0.0294

SEQ_EX
  mean             0.0067  0.6651
  std. dev.         0.064  0.9658
  weight sum         1042    1040
  precision        0.0296  0.0296

SEQ_THRE
  NO               1026.0    44.0
  YES                18.0   998.0
  [total]          1044.0  1042.0

HAS_CD
  YES              1028.0  1021.0
  NO                 16.0    21.0
  [total]          1044.0  1042.0

CD_HIT
  YES              1028.0  1019.0
  NO                 16.0    23.0
  [total]          1044.0  1042.0

CD_AP
  mean             0.3677  0.0197
  std. dev.         0.442  0.0221
  weight sum         1042    1040
  precision        0.0172  0.0172

CD_ID
  mean             0.9388  0.9122
  std. dev.        0.1333  0.1524
  weight sum         1042    1040
  precision        0.0357  0.0357

CD_EX
  mean             0.3735  0.9417
  std. dev.        0.7914  1.2691
  weight sum         1042    1040
  precision         0.029   0.029

CD_THRE
  YES               561.0  1036.0
  NO                483.0     6.0
  [total]          1044.0  1042.0

HAS_TR
  YES              1028.0  1021.0
  NO                 16.0    21.0
  [total]          1044.0  1042.0

TR_HIT
  YES               910.0   593.0
  NO                134.0   449.0
  [total]          1044.0  1042.0

TR_AP
  mean             0.3662  0.0153
  std. dev.        0.4415   0.029
  weight sum         1042    1040
  precision        0.0159  0.0159

TR_ID
  mean             0.6296   0.214
  std. dev.        0.3735  0.2106
  weight sum         1042    1040
  precision        0.0154  0.0154

TR_EX
  mean             1.4731  3.1773
  std. dev.        2.2145  2.1828
  weight sum         1042    1040
  precision        0.0289  0.0289

TR_THRE
  YES               581.0  1035.0
  NO                463.0     7.0
  [total]          1044.0  1042.0


